        var msg = document.getElementById('msg');
        var form = document.getElementById('form1');
        form.addEventListener('submit', function (event) {
            event.preventDefault();
            var inputs = document.querySelectorAll('#form1 input');
            var msg = document.getElementById('msg');
            msg.innerHTML = " ";
            for (i = 0; i < inputs.length; i++) {
                if (inputs[i].name == 'login') {
                    if (inputs[i].value == 'admin') {
                        msg.innerHTML += '<h1>"Привет Админинстратор!"</h1>';
                    }
                    else {
                        msg.innerHTML += '<h1>Привет пользователь:' + ' ' + inputs[i].value + '</h1>';
                    }
                }
                if (inputs[i].name == 'password') {
                    msg.innerHTML += '<p>Ваш пароль:' + ' ' + inputs[i].value + '</p>';
                }
            }
        });
        /* ----------------------------------------------------------------------------------
        -------------------------------- My CODE --------------------------------------------
        -------------------------------------------------------------------------------------
        */
        // Проверка введённых символов
        var logkey = document.getElementById('logkey');
        logkey.addEventListener('keypress', function (event) {
            var error = document.getElementById('error-msg');
            if (!(event.keyCode >= 65 && event.keyCode <= 122)) {
                error.innerHTML = 'Switch the keyboard language of the computer!';
                logkey.value = '';
                event.preventDefault();
            }
            else {
                error.innerHTML = '';
            }
        });
        // Вывод ввода пароля по буквам
        var pass = document.getElementById('pass');
        var counter = 0;
        pass.addEventListener('keypress', function (event) {
            var msg = document.getElementById('msg');
            if (counter == 0) {
                msg.innerHTML += 'You entered: ' + String.fromCharCode(event.keyCode);
            }
            else {
                msg.innerHTML += String.fromCharCode(event.keyCode);
            }
            counter++;
        });